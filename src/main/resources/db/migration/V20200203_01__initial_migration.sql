CREATE TABLE IF NOT EXISTS `access_log` (
	`id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`datetime` timestamp,
	`origin` VARCHAR(36),
	`requested_url` VARCHAR(256)
);