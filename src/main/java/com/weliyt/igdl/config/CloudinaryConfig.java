package com.weliyt.igdl.config;

import com.cloudinary.Cloudinary;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
public class CloudinaryConfig {

    private final boolean isEnabled;
    private final String cloudName;
    private final String apiKey;
    private final String apiSecret;

    public CloudinaryConfig(@Value("${cloudinary.enabled:false}") final boolean isEnabled,
                            @Value("${cloudinary.cloud-name}") final String cloudName,
                            @Value("${cloudinary.api-key}") final String apiKey,
                            @Value("${cloudinary.api-secret}") final String apiSecret) {
        this.isEnabled = isEnabled;
        this.cloudName = cloudName;
        this.apiKey = apiKey;
        this.apiSecret = apiSecret;
    }

    @Bean
    public Cloudinary getCloudinary() {
        if (isEnabled) {
            Map<String, String> cfg = new HashMap<>();
            cfg.put("cloud_name", cloudName);
            cfg.put("api_key", apiKey);
            cfg.put("api_secret", apiSecret);

            return new Cloudinary(cfg);
        } else {
            log.info("Cloudinary upload is disabled");
            return new Cloudinary();
        }
    }

}
