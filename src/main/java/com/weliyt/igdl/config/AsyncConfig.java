package com.weliyt.igdl.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@EnableAsync
@Configuration
public class AsyncConfig {

    @Primary
    @Bean
    public Executor getExecutor() {
        ThreadPoolTaskExecutor ex = new ThreadPoolTaskExecutor();
        ex.setCorePoolSize(5);
        ex.setMaxPoolSize(25);
        ex.setThreadNamePrefix("asynclog-pool-");
        ex.initialize();
        return ex;
    }

    @Bean(name="cloudinaryExecutor")
    public Executor getCloudinaryExecutor() {
        ThreadPoolTaskExecutor ex = new ThreadPoolTaskExecutor();
        ex.setCorePoolSize(5);
        ex.setMaxPoolSize(50);
        ex.setThreadNamePrefix("cloudinary-pool-");
        ex.initialize();
        return ex;
    }

}
