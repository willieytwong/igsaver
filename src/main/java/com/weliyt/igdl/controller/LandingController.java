package com.weliyt.igdl.controller;

import com.weliyt.igdl.model.SimpleResponse;
import com.weliyt.igdl.service.IGExtractionService;
import com.weliyt.igdl.service.LogService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@AllArgsConstructor
@Slf4j
@Controller
public class LandingController {

    private LogService logService;
    private IGExtractionService igExtractionService;

    @RequestMapping("")
    public String doLanding() {
        return "index";
    }

    @PostMapping("/dofetch")
    public ResponseEntity<SimpleResponse> doFetch(final HttpServletRequest req, @RequestBody final String url) {
        logService.doLog(getClientIp(req), url);

        var ret = igExtractionService.execute(url);
        return ResponseEntity.ok(ret);
    }

    private static String getClientIp(final HttpServletRequest req) {
        var remoteAddr = "";

        if (req != null) {
            remoteAddr = req.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = req.getRemoteAddr();
            }
        }
        return remoteAddr;
    }

}
