package com.weliyt.igdl.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

@Slf4j
@Service
public class IGWebConnectionService {

    private final String cookieValue;

    public IGWebConnectionService(@Value("${instagram.cookie}") final String cookieValue) {
        this.cookieValue = cookieValue;
    }

    public String fetchHtmlAsString(final String urlString) {
        var ret = Strings.EMPTY;

        try {
            var url = new URL(urlString);
            var conn = url.openConnection();
            conn.setRequestProperty("Cookie", cookieValue);
            conn.setUseCaches(true);

            try (var bis = new BufferedInputStream(conn.getInputStream(), 32 * 1024 * 1024)) {
                ret = IOUtils.toString(bis, StandardCharsets.UTF_8);
            }

        } catch (IOException e) {
            log.error("Error connecting to URL: " + urlString);
        }

        return ret;
    }

}
