package com.weliyt.igdl.service;

import com.cloudinary.Cloudinary;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CloudinaryService implements CloudUploadService {

    private final Cloudinary cloudinary;
    private final boolean isEnabled;

    private final Map<String, String> uploadOpts;

    public CloudinaryService(final Cloudinary cloudinary,
                             @Value("${cloudinary.enabled:false}") final boolean isEnabled,
                             @Value("${cloudinary.upload.directory:/}") final String uploadDirectory) {
        this.cloudinary = cloudinary;
        this.isEnabled = isEnabled;

        var uploadOptions = new HashMap<String, String>();
        uploadOptions.put("resource_type", "image");
        uploadOptions.put("folder", uploadDirectory);

        this.uploadOpts = Collections.unmodifiableMap(uploadOptions);
    }

    @Async("cloudinaryExecutor")
    public CompletableFuture<String> upload(final String url, final String pid) {
        if (cloudinary == null || !isEnabled) return CompletableFuture.completedFuture(url);

        try {

            var opts = new HashMap<>(uploadOpts);
            opts.put("context", "source=instagram|pid=" + pid);

            var resp = cloudinary.uploader().upload(url, opts);

            return CompletableFuture.completedFuture(String.valueOf(resp.get("secure_url")));
        } catch (IOException e) {
            log.error("Error uploading asset to Cloudinary: {}", e.getMessage());
        }

        return CompletableFuture.completedFuture(url);
    }

    public List<String> search(final String pid) {
        if (cloudinary == null || !isEnabled) return Collections.emptyList();

        var ret = new ArrayList<String>();

        try {
            var resp = cloudinary.search()
                    .expression("context.source=instagram AND context.pid=" + pid)
                    .execute();

            if (resp != null && resp.containsKey("resources")) {
                List<HashMap<String, String>> resources = (List<HashMap<String, String>>) resp.get("resources");
                for (var res : resources) {
                    ret.add(res.get("secure_url"));
                }

            }

            return ret.stream().filter(Objects::nonNull).collect(Collectors.toList());

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return ret;
    }
}
