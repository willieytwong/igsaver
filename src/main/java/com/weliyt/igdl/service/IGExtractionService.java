package com.weliyt.igdl.service;

import com.weliyt.igdl.model.SimpleResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.util.Strings;
import org.assertj.core.util.Sets;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@AllArgsConstructor
@Slf4j
@Service
public class IGExtractionService {

    private static final String IG_POST_URL = "https://www.instagram.com/p/%s";
    private static final Pattern URL_PATTERN = Pattern.compile("^(?:https?:\\/\\/)?(?:www\\.)?instagram\\.com\\/p\\/([^\\?\\/]+)");
    private static final Pattern IMAGE_PATTERN = Pattern.compile("\\\"display_url\\\"\\s*:\\s*\\\"(.+?)\\\"");
    private static final Pattern VIDEO_PATTERN = Pattern.compile("\\\"video_url\\\"\\s*:\\s*\\\"(.+?)\\\"");

    private final CloudUploadService cloudUploadService;
    private final IGWebConnectionService igWebConnectionService;

    public SimpleResponse execute(final String url) {
        var ret = new SimpleResponse();

        if (StringUtils.isBlank(url)) return ret;

        var pid = extractPid(url);
        if (StringUtils.isBlank(pid)) return ret;

        long fetchStart = System.currentTimeMillis();
        var html = fetchBody(pid);
        log.info("Instagram Fetch Time: {} ms", System.currentTimeMillis() - fetchStart);


        //Search to see if Cloud has a cache of images first
        long searchStart = System.currentTimeMillis();
        Set<String> imgUrls = Sets.newHashSet(cloudUploadService.search(pid));
        log.info("Cloud Search Time: {} ms", System.currentTimeMillis() - searchStart);


        if (CollectionUtils.isEmpty(imgUrls)) {
            long uploadStart = System.currentTimeMillis();

            imgUrls = extractImageUrls(html).stream()
                    .map(imgUrl -> cloudUploadService.upload(imgUrl, pid))
                    .collect(Collectors.toList())
                    .stream()
                    .map(CompletableFuture::join)
                    .collect(Collectors.toUnmodifiableSet());

            log.info("Cloud Upload Time: {} ms", System.currentTimeMillis() - uploadStart);
        }

        ret.setImgUrls(imgUrls);
        ret.setVidUrls(extractVideoUrls(html));

        return ret;
    }

    private String extractPid(final String in) {
        String ret = null;

        var matcher = URL_PATTERN.matcher(in);
        if (matcher.find()) {
            ret = matcher.group(1);
        }

        return ret;
    }

    private String fetchBody(final String pid) {
        var ret = Strings.EMPTY;

        var decoded = URLDecoder.decode(pid, StandardCharsets.UTF_8);

        var url = String.format(IG_POST_URL, decoded);
        ret = igWebConnectionService.fetchHtmlAsString(url);

        return ret;
    }

    private Set<String> extractImageUrls(final String html) {
        var ret = new TreeSet<String>();

        var matcher = IMAGE_PATTERN.matcher(html);
        while (matcher.find()) {
            ret.add(StringEscapeUtils.unescapeJava(matcher.group(1)));
        }

        return ret;
    }

    private Set<String> extractVideoUrls(final String html) {
        var ret = new TreeSet<String>();

        var matcher = VIDEO_PATTERN.matcher(html);
        while (matcher.find()) {
            ret.add(StringEscapeUtils.unescapeJava(matcher.group(1)));
        }

        return ret;
    }

}
