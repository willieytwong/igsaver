package com.weliyt.igdl.service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface CloudUploadService {

    CompletableFuture<String> upload(final String url, final String tags);

    List<String> search(final String pid);

}
