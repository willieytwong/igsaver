package com.weliyt.igdl.service;

import com.cloudinary.Cloudinary;
import com.cloudinary.Uploader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

@RunWith(MockitoJUnitRunner.class)
public class CloudinaryServiceTest {

    @Mock
    private Cloudinary cloudinary;
    @Mock
    private Uploader uploader;

    private CloudinaryService enabledSvc;
    private CloudinaryService disabledSvc;

    @Before
    public void init() {
        enabledSvc = new CloudinaryService(cloudinary, true, "/upload-dir/");
        disabledSvc = new CloudinaryService(null, false, "/upload-dir/");
    }

    @Test
    public void testUpload_Disabled() throws ExecutionException, InterruptedException {
        final String url = "https://test-disabled-upload-url.test";

        String ret = disabledSvc.upload(url, "tags").get();
        assertEquals(url, ret);
    }

    @Test
    public void testUpload_Enabled_UploadFailed() throws ExecutionException, InterruptedException, IOException {
        final String url = "https://test-failed-upload-url.test";

        doReturn(uploader).when(cloudinary).uploader();
        doThrow(new IOException()).when(uploader).upload(any(), any());

        String ret = enabledSvc.upload(url, "tags").get();
        assertEquals(url, ret);
    }


    @Test
    public void testUpload_Enabled_UploadSuccess() throws ExecutionException, InterruptedException, IOException {
        final String url = "https://test-successful-upload-url.test";
        final String newUrl = "https://cloudinary-url.test";

        var returnedMap = Map.of("secure_url", newUrl);

        doReturn(uploader).when(cloudinary).uploader();
        doReturn(returnedMap).when(uploader).upload(any(), any());

        String ret = enabledSvc.upload(url, "tags").get();
        assertEquals(newUrl, ret);
    }
}
