package com.weliyt.igdl.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Slf4j
@RunWith(JUnit4.class)
public class IGWebConnectionServiceTest {

    private IGWebConnectionService IGWebConnectionService;

    @Before
    public void before() {
        this.IGWebConnectionService = new IGWebConnectionService("cookie");
    }

    @Test
    public void testFetchRegularWebpage() {
        var url = "https://www.google.ca/";

        var ret = IGWebConnectionService.fetchHtmlAsString(url);
        assertFalse(ret.isEmpty());
    }

    @Test
    public void testNotAWebpage() {
        var url = "http://localhost/definitely-not-a-valid-url-will-timeout";

        var ret = IGWebConnectionService.fetchHtmlAsString(url);
        assertTrue(ret.isEmpty());
    }

}
