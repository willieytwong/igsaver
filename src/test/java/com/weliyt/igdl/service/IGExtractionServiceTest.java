package com.weliyt.igdl.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class IGExtractionServiceTest {

    private static final String[] validUrls = {
            "https://instagram.com/p/B7bKTTNB_-m/",
            "https://instagram.com/p/B7bKTTNB_-m",
            "https://www.instagram.com/p/B7bKTTNB_-m/",
            "https://www.instagram.com/p/B7bKTTNB_-m",
            "https://www.instagram.com/p/B7bKTTNB_-m?param1=param1value",
            "http://instagram.com/p/B7bKTTNB_-m/",
            "http://instagram.com/p/B7bKTTNB_-m",
            "http://instagram.com/p/B7bKTTNB_-m?param1=param1value",
            "http://www.instagram.com/p/B7bKTTNB_-m/",
            "http://www.instagram.com/p/B7bKTTNB_-m",
            "http://www.instagram.com/p/B7bKTTNB_-m?param1=param1value",
            "www.instagram.com/p/B7bKTTNB_-m",
            "www.instagram.com/p/B7bKTTNB_-m/param1=param1value",
            "www.instagram.com/p/B7bKTTNB_-m/",
            "www.instagram.com/p/B7bKTTNB_-m/?param1=param1value",
            "instagram.com/p/B7bKTTNB_-m",
            "instagram.com/p/B7bKTTNB_-m?param1=param1value",
            "instagram.com/p/B7bKTTNB_-m/",
            "instagram.com/p/B7bKTTNB_-m/?param1=param1value"
    };

    private static String validIgPostBody;

    @Mock
    private IGWebConnectionService IGWebConnectionService;
    @Mock
    private CloudUploadService cloudinaryService;

    @InjectMocks
    private IGExtractionService igExtractionService;

    @BeforeClass
    public static void init() {
        try {
            validIgPostBody = IOUtils.toString(Objects.requireNonNull(IGExtractionServiceTest.class.getClassLoader().getResourceAsStream("ValidIGPostBody.html")), StandardCharsets.UTF_8);
        } catch (Exception e) {
            log.error("Unable to load resource ValidIGPostBody.html", e);
            validIgPostBody = null;
        }
    }

    @Test
    public void testExecute_EmptyUrl() {
        var result = igExtractionService.execute("");

        assertTrue(result.getImgUrls().isEmpty());
        assertTrue(result.getVidUrls().isEmpty());
    }

    @Test
    public void testExecute_NullUrl() {
        var result = igExtractionService.execute(null);

        assertTrue(result.getImgUrls().isEmpty());
        assertTrue(result.getVidUrls().isEmpty());
    }

    @Test
    public void testExecute_ValidIGUrls() {
        doReturn(validIgPostBody).when(IGWebConnectionService).fetchHtmlAsString(any());
        doReturn(CompletableFuture.completedFuture("https://link-to-image")).when(cloudinaryService).upload(anyString(), anyString());

        Arrays.stream(validUrls).forEach(u -> {
            var result = igExtractionService.execute(u);
            log.info(String.format("Testing %s, parsed results: %s", u, result.toString()));

            assertFalse(result.getImgUrls().isEmpty());
            assertFalse(result.getVidUrls().isEmpty());
        });
    }
}
