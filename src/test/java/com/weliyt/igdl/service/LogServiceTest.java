package com.weliyt.igdl.service;

import com.weliyt.igdl.entity.AccessLogEntry;
import com.weliyt.igdl.repo.AccessLogRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LogServiceTest {

    @Mock
    private AccessLogRepo repo;

    private LogService svc;

    @Test
    public void testDoLog_Entry_Disabled() {
        svc = new LogService(false, repo);

        svc.doLog(new AccessLogEntry());
        verify(repo, never()).save(any(AccessLogEntry.class));
    }

    @Test
    public void testDoLog_Entry_Enabled() {
        svc = new LogService(true, repo);
        doReturn(null).when(repo).save(any(AccessLogEntry.class));

        svc.doLog(new AccessLogEntry());
        verify(repo).save(any(AccessLogEntry.class));
    }

    @Test
    public void testDoLog_Strings_Disabled() {
        svc = new LogService(false, repo);

        svc.doLog("origin_string", "requester_string");
        verify(repo, never()).save(any(AccessLogEntry.class));
    }

    @Test
    public void testDoLog_Strings_Enabled() {
        svc = new LogService(true, repo);
        doReturn(null).when(repo).save(any(AccessLogEntry.class));

        svc.doLog("origin_string", "requester_string");
        verify(repo).save(any(AccessLogEntry.class));
    }
}
